package cz.kaserdan.epoxytest.epoxy

import android.widget.TextView
import butterknife.BindView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import cz.kaserdan.epoxytest.R

@EpoxyModelClass(layout = R.layout.header_view)
abstract class HeaderModel : EpoxyModelWithHolder<HeaderModel.Holder>() {
    @EpoxyAttribute
    var title: String? = null

    override fun bind(holder: Holder) {
        holder.header.text = title
    }

    class Holder : BaseEpoxyHolder() {
        @BindView(R.id.text)
        lateinit var header: TextView
    }
}