package cz.kaserdan.epoxytest.epoxy

import android.widget.TextView
import butterknife.BindView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import cz.kaserdan.epoxytest.R

@EpoxyModelClass(layout = R.layout.description_view)
abstract class DescriptionModel : EpoxyModelWithHolder<DescriptionModel.Holder>() {
    @EpoxyAttribute
    var text: String? = null

    override fun bind(holder: Holder) {
        holder.description.text = text
    }

    class Holder : BaseEpoxyHolder() {
        @BindView(R.id.text)
        lateinit var description: TextView
    }
}