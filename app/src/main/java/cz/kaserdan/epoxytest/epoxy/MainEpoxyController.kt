package cz.kaserdan.epoxytest.epoxy

import com.airbnb.epoxy.AutoModel
import com.airbnb.epoxy.EpoxyController


class MainEpoxyController : EpoxyController() {

    private var count = 2
    @AutoModel
    lateinit var footer: FooterModel_

    fun addItem() {
        this.count += 2
        requestModelBuild()
    }

    override fun buildModels() {
        for (idx in 1..count) {
            if (idx.rem(2) == 1) {
                header {
                    id(idx)
                    title("This header number ${(idx / 2) + 1}")
                }
            } else {
                description {
                    id(idx)
                    text("Random descrition blablabla ${idx / 2}")
                }
            }
        }
        footer.addTo(this)
    }

}