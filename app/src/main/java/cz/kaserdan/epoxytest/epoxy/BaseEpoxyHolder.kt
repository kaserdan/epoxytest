package cz.kaserdan.epoxytest.epoxy

import android.view.View
import butterknife.ButterKnife
import com.airbnb.epoxy.EpoxyHolder

open class BaseEpoxyHolder : EpoxyHolder() {
    override fun bindView(itemView: View) {
        ButterKnife.bind(this, itemView)
    }
}
