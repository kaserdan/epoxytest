package cz.kaserdan.epoxytest.epoxy

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import cz.kaserdan.epoxytest.R

@EpoxyModelClass(layout = R.layout.footer_view)
abstract class FooterModel : EpoxyModelWithHolder<BaseEpoxyHolder>()