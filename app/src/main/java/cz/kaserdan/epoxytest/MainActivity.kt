package cz.kaserdan.epoxytest

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import cz.kaserdan.epoxytest.epoxy.MainEpoxyController
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val controller = MainEpoxyController()
        epoxyRecyclerView.layoutManager = LinearLayoutManager(this)
        epoxyRecyclerView.setController(controller)
        controller.requestModelBuild()
        fab.setOnClickListener { controller.addItem() }
    }
}
